package ru.tsc.bagrintsev.tm.api.sevice;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.command.AbstractCommand;
import ru.tsc.bagrintsev.tm.exception.AbstractException;

import java.util.Collection;

public interface ICommandService extends Checkable {

    void add(@Nullable final AbstractCommand command);

    @NotNull
    Collection<AbstractCommand> getAvailableCommands();

    @NotNull
    AbstractCommand getCommandByName(@Nullable final String name) throws AbstractException;

    @NotNull
    AbstractCommand getCommandByShort(@Nullable final String shortName) throws AbstractException;

    @NotNull
    Iterable<AbstractCommand> getShortCommands();

}
