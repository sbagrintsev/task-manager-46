package ru.tsc.bagrintsev.tm.api.endpoint;

import jakarta.jws.WebMethod;
import jakarta.jws.WebParam;
import jakarta.jws.WebService;
import jakarta.xml.bind.JAXBException;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.dto.request.data.*;
import ru.tsc.bagrintsev.tm.dto.response.data.*;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.DomainNotFoundException;

import java.io.IOException;
import java.sql.SQLException;

@WebService
public interface IDomainEndpoint extends IAbstractEndpoint {

    @NotNull
    String NAME = "DomainEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IDomainEndpoint newInstance(
            @NotNull final String host,
            @NotNull final String port
    ) {
        return IAbstractEndpoint.newInstance(host, port, NAME, SPACE, PART, IDomainEndpoint.class);
    }

    @NotNull
    @WebMethod
    BackupLoadResponse loadBackup(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final BackupLoadRequest request
    ) throws SQLException, DomainNotFoundException, IOException, ClassNotFoundException;

    @NotNull
    @WebMethod
    DataBase64LoadResponse loadBase64(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBase64LoadRequest request
    ) throws SQLException, DomainNotFoundException, IOException, ClassNotFoundException;

    @NotNull
    @WebMethod
    DataBinaryLoadResponse loadBinary(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBinaryLoadRequest request
    ) throws SQLException, DomainNotFoundException, IOException, ClassNotFoundException;

    @NotNull
    @WebMethod
    DataJacksonJsonLoadResponse loadJacksonJson(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJacksonJsonLoadRequest request
    ) throws SQLException, DomainNotFoundException, IOException;

    @NotNull
    @WebMethod
    DataJacksonXmlLoadResponse loadJacksonXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJacksonXmlLoadRequest request
    ) throws SQLException, DomainNotFoundException, IOException;

    @NotNull
    @WebMethod
    DataJacksonYamlLoadResponse loadJacksonYaml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJacksonYamlLoadRequest request
    ) throws SQLException, DomainNotFoundException, IOException;

    @NotNull
    @WebMethod
    DataJaxbJsonLoadResponse loadJaxbJson(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJaxbJsonLoadRequest request
    ) throws SQLException, JAXBException, DomainNotFoundException;

    @NotNull
    @WebMethod
    DataJaxbXmlLoadResponse loadJaxbXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJaxbXmlLoadRequest request
    ) throws SQLException, JAXBException, DomainNotFoundException;

    @NotNull
    @WebMethod
    BackupSaveResponse saveBackup(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final BackupSaveRequest request
    ) throws SQLException, AbstractException, IOException;

    @NotNull
    @WebMethod
    DataBase64SaveResponse saveBase64(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBase64SaveRequest request
    ) throws SQLException, AbstractException, IOException;

    @NotNull
    @WebMethod
    DataBinarySaveResponse saveBinary(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBinarySaveRequest request
    ) throws SQLException, AbstractException, IOException;

    @NotNull
    @WebMethod
    DataJacksonJsonSaveResponse saveJacksonJson(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJacksonJsonSaveRequest request
    ) throws SQLException, AbstractException, IOException;

    @NotNull
    @WebMethod
    DataJacksonXmlSaveResponse saveJacksonXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJacksonXmlSaveRequest request
    ) throws SQLException, AbstractException, IOException;

    @NotNull
    @WebMethod
    DataJacksonYamlSaveResponse saveJacksonYaml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJacksonYamlSaveRequest request
    ) throws SQLException, AbstractException, IOException;

    @NotNull
    @WebMethod
    DataJaxbJsonSaveResponse saveJaxbJson(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJaxbJsonSaveRequest request
    ) throws SQLException, AbstractException, JAXBException;

    @NotNull
    @WebMethod
    DataJaxbXmlSaveResponse saveJaxbXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJaxbXmlSaveRequest request
    ) throws SQLException, AbstractException, JAXBException;

}
