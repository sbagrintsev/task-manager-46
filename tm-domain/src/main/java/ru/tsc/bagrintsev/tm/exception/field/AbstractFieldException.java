package ru.tsc.bagrintsev.tm.exception.field;

import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.exception.AbstractException;

public abstract class AbstractFieldException extends AbstractException {

    public AbstractFieldException() {
        super();
    }

    public AbstractFieldException(@Nullable final String message) {
        super(message);
    }

    public AbstractFieldException(
            @Nullable final String message,
            @Nullable final Throwable cause
    ) {
        super(message, cause);
    }

    public AbstractFieldException(@Nullable final Throwable cause) {
        super(cause);
    }

    protected AbstractFieldException(
            @Nullable final String message,
            @Nullable final Throwable cause,
            final boolean enableSuppression,
            final boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
