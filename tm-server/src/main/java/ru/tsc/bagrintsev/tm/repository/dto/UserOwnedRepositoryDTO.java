package ru.tsc.bagrintsev.tm.repository.dto;

import jakarta.persistence.EntityManager;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.api.repository.dto.IUserOwnedRepositoryDTO;
import ru.tsc.bagrintsev.tm.dto.model.AbstractWBSModelDTO;

import java.util.List;

public class UserOwnedRepositoryDTO<M extends AbstractWBSModelDTO> extends AbstractRepositoryDTO<M> implements IUserOwnedRepositoryDTO<M> {

    public UserOwnedRepositoryDTO(
            @NotNull final Class<M> clazz,
            @NotNull final EntityManager entityManager
    ) {
        super(clazz, entityManager);
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final String jpql = String.format("DELETE FROM %s m WHERE m.userId = :userId", clazz.getSimpleName());
        entityManager
                .createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public boolean existsById(
            @NotNull final String userId,
            @NotNull final String id
    ) {
        @NotNull final String jpql = String.format(
                "SELECT count(*) = 1 FROM %s m WHERE m.id = :id AND m.userId = :userId",
                clazz.getSimpleName());
        return entityManager
                .createQuery(jpql, Boolean.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @Override
    public @Nullable List<M> findAllByUserId(@NotNull final String userId) {
        @NotNull final String jpql = String.format("FROM %s m WHERE m.userId = :userId", clazz.getSimpleName());
        return entityManager
                .createQuery(jpql, clazz)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public @Nullable List<M> findAllSort(
            @NotNull final String userId,
            @NotNull final String order
    ) {
        @NotNull final String jpql = String.format(
                "FROM %s m WHERE m.userId = :userId ORDER BY :order",
                clazz.getSimpleName());
        return entityManager
                .createQuery(jpql, clazz)
                .setParameter("userId", userId)
                .setParameter("order", order)
                .getResultList();
    }

    @Override
    public @Nullable M findOneById(
            @NotNull final String userId,
            @NotNull final String id
    ) {
        @NotNull final String jpql = String.format(
                "FROM %s m WHERE m.id = :id AND m.userId = :userId",
                clazz.getSimpleName());
        @NotNull final List<M> result = entityManager
                .createQuery(jpql, clazz)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getResultList();
        if (result.isEmpty()) return null;
        return result.get(0);
    }

    @Override
    public void removeById(
            @NotNull final String userId,
            @NotNull final String id
    ) {
        @NotNull final String jpql = String.format(
                "DELETE FROM %s m WHERE m.userId = :userId AND m.id = :id",
                clazz.getSimpleName());
        entityManager
                .createQuery(jpql)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public long totalCountByUserId(@NotNull final String userId) {
        @NotNull final String jpql = String.format(
                "SELECT count(*) FROM %s m WHERE m.userId = :userId",
                clazz.getSimpleName());
        return entityManager
                .createQuery(jpql, Long.class)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @Override
    public void update(@NotNull final M record) {
        entityManager.merge(record);
    }

    @Override
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        @NotNull final String jpql = String.format("" +
                        "UPDATE %s m " +
                        "SET m.name = :name, " +
                        "m.description = :description " +
                        "WHERE m.userId = :userId " +
                        "AND m.id = :id",
                clazz.getSimpleName());
        entityManager
                .createQuery(jpql)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setParameter("name", name)
                .setParameter("description", description)
                .executeUpdate();
    }

}
